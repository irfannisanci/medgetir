package com.unicorn.medgetir.business.concretes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unicorn.medgetir.business.abstracts.ChemistService;
import com.unicorn.medgetir.business.constant.Messages;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.ErrorDataResult;
import com.unicorn.medgetir.core.utilities.results.ErrorResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.core.utilities.results.SuccessDataResult;
import com.unicorn.medgetir.core.utilities.results.SuccessResult;
import com.unicorn.medgetir.dto.ChemistDto;
import com.unicorn.medgetir.model.Chemist;
import com.unicorn.medgetir.repository.ChemistRepository;

@Service
public class ChemistManager implements ChemistService {

	@Autowired
	private ChemistRepository chemistRepository;
	
	@Override
	public DataResult<Chemist> createChemist(ChemistDto chemistDto) {
		if (chemistRepository.existsByPhone(chemistDto.getPhone())) {
			return new ErrorDataResult<Chemist>(Messages.existPhone);
		}
		else {
			Chemist newChemist=new Chemist();
			newChemist.setId(chemistDto.getId());
			newChemist.setChemistName(chemistDto.getChemistName());
			newChemist.setPhone(chemistDto.getPhone());
			newChemist.setPassword(chemistDto.getPassword());
			return new SuccessDataResult<Chemist>(chemistRepository.save(newChemist));
		}
	}

	@Override
	public DataResult<List<Chemist>> getAllChemist() {
		return new SuccessDataResult<List<Chemist>>(chemistRepository.findAll());
	}

	@Override
	public DataResult<Chemist> updateChemist(ChemistDto chemistDto) {
		if (chemistRepository.existsByPhone(chemistDto.getPhone())) {
			return new ErrorDataResult<Chemist>(Messages.existPhone);
		}
		else if(!chemistRepository.existsById(chemistDto.getId())){
			return new ErrorDataResult<Chemist>(Messages.userNotFound);
		}
		else {
			Chemist newChemist=chemistRepository.getById(chemistDto.getId());
			newChemist.setChemistName(chemistDto.getChemistName());
			newChemist.setPhone(chemistDto.getPhone());
			newChemist.setPassword(chemistDto.getPassword());
			return new SuccessDataResult<Chemist>(chemistRepository.save(newChemist),Messages.updateUser);
		}
	}

	@Override
	public Result deleteChemist(int id) {
		if (chemistRepository.existsById(id)) {
			chemistRepository.deleteById(id);
			return new SuccessResult(Messages.deleteUser);
		}
		else {
			return new ErrorResult(Messages.userNotFound);
		}
		
	}

}
