package com.unicorn.medgetir.business.concretes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unicorn.medgetir.business.abstracts.UserService;
import com.unicorn.medgetir.business.constant.Messages;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.ErrorDataResult;
import com.unicorn.medgetir.core.utilities.results.ErrorResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.core.utilities.results.SuccessDataResult;
import com.unicorn.medgetir.core.utilities.results.SuccessResult;
import com.unicorn.medgetir.dto.UserDTO;
import com.unicorn.medgetir.model.User;
import com.unicorn.medgetir.repository.UserRepository;
@Service
public class UserManager implements UserService {

	
	@Autowired
	private UserRepository userRepository;
	
	@Override
	public DataResult<List<User>> getAllUser() {
	return new SuccessDataResult<List<User>>(userRepository.findAll(),Messages.getAllUsers);
	}
	
	@Override
	public Result add(UserDTO userDTO) {
		if (userRepository.existsByPhone(userDTO.getPhone())) {
			return new ErrorResult(Messages.existPhone);
		}
		else if (userRepository.existsByNationalityId(userDTO.getNationalityId())) {
			return new ErrorResult(Messages.existNationalityId);
		}
		else {
			User newUser =new User();
			newUser.setId(userDTO.getId());
			newUser.setName(userDTO.getName());
			newUser.setSurname(userDTO.getSurname());
			newUser.setPhone(userDTO.getPhone());
			newUser.setPassword(userDTO.getPassword());
			newUser.setNationalityId(userDTO.getNationalityId());
			newUser.setDateOfBirth(userDTO.getDateOfBirth());
			userRepository.save(newUser);
			return new SuccessResult(Messages.createdUser);
			/*User user=new User();
			BeanUtils.copyProperties(userDTO, user);
			User newUser=userRepository.save(user);
			return newUser;*/
		}
		
	}

	@Override
	public DataResult<User> updateUser(UserDTO userDTO) {
		if (userRepository.existsByPhone(userDTO.getPhone())) {
			return new ErrorDataResult<User>(Messages.existPhone);
		}
		else if (userRepository.existsByNationalityId(userDTO.getNationalityId())) {
			return new ErrorDataResult<User>(Messages.existNationalityId);
		}
		else if (!userRepository.existsById(userDTO.getId())) {
			
			return new ErrorDataResult<User>(Messages.userNotFound);
		}else {
			User newUser=userRepository.getById(userDTO.getId());
			newUser.setName(userDTO.getName());
			newUser.setSurname(userDTO.getSurname());
			newUser.setPassword(userDTO.getPassword());
			newUser.setNationalityId(userDTO.getNationalityId());
			newUser.setDateOfBirth(userDTO.getDateOfBirth());
			newUser.setPhone(userDTO.getPhone());
			return new SuccessDataResult<User>(userRepository.save(newUser),Messages.updateUser);
		}
	}
	
	@Override
	public Result deleteUser(int id) {
		if (userRepository.existsById(id)) {
			userRepository.deleteById(id);
			 return new SuccessResult(Messages.deleteUser);
		}
		else {
			return new ErrorResult(Messages.userNotFound);
		}
	}

	@Override
	public DataResult<User> findByPhone(String phone) {
		
		return new SuccessDataResult<User>(userRepository.findByPhone(phone));
	}

	@Override
	public DataResult<User> getById(int id) {
		
		return new SuccessDataResult<User>(userRepository.getById(id));
	}

	@Override
	public DataResult<User> findByPhoneAndPassword(String phone, String password) {
		return new SuccessDataResult<User>(userRepository.findByPhoneAndPassword(phone, password));
	}
}
