package com.unicorn.medgetir.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unicorn.medgetir.model.User;

public interface UserRepository extends JpaRepository<User,Integer> {

	User findByName(String name);
	User findByname(String name);
	User findByPhone(String phone);
	User getById(int id);
	User findByPhoneAndPassword(String phone,String password);
	Boolean existsByPhone(String phone);
	Boolean existsByNationalityId(String NationalityId);
}
