package com.unicorn.medgetir.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name ="Adress")
@AllArgsConstructor
@NoArgsConstructor

public class Adress {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotBlank(message = "İl alanı boş olamaz")
	private String il;
	
	@NotBlank(message = "İlçe alanı boş olamaz")
	private String ilce;
	
	@NotBlank(message = "Mahalle alanı boş olamaz")
	private String mahalle;
	
	@NotBlank(message = "Cadde alanı boş olamaz")
	private String cadde;
	private String sokak;
	
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id",referencedColumnName = "id")
	private User users;
}
