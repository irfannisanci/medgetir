package com.unicorn.medgetir.business.abstracts;

import java.util.List;

import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.PrescriptionsDto;
import com.unicorn.medgetir.model.Prescriptions;

public interface PrescriptionsService {

	Result addPrescription(PrescriptionsDto prescriptionsDto);
	DataResult<List<Prescriptions>> getPrescriptionByUserId(int id);
	DataResult<List<Prescriptions>> getPrescriptionByChemistsId(int id);
}
