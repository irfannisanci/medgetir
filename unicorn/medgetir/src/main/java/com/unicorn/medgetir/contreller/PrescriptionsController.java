package com.unicorn.medgetir.contreller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unicorn.medgetir.business.concretes.PrescriptionsManager;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.PrescriptionsDto;
import com.unicorn.medgetir.model.Prescriptions;

@RestController
@RequestMapping("/prescriptions")
@CrossOrigin
public class PrescriptionsController {

	@Autowired
	private PrescriptionsManager prescriptionsManager;
	
	@PostMapping("/addPrescription")
	public Result addPrescription(@Valid @RequestBody PrescriptionsDto prescriptionsDto) {
		return prescriptionsManager.addPrescription(prescriptionsDto);
	}
	
	@GetMapping("/getPrescriptionByUserId")
	public DataResult<List<Prescriptions>> getByUserId(@RequestParam int id){
		return prescriptionsManager.getPrescriptionByUserId(id);
	}
	
	@GetMapping("/getPrescriptionByChemistId")
	public DataResult<List<Prescriptions>> getPrescriptionByChemistsId(@RequestParam int id){
		return prescriptionsManager.getPrescriptionByChemistsId(id);
	}
}
