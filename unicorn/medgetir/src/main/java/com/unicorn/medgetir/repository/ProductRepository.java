package com.unicorn.medgetir.repository;

import com.unicorn.medgetir.model.Product;

import org.springframework.data.jpa.repository.JpaRepository;

import javax.persistence.Id;
import java.util.List;

public interface ProductRepository extends JpaRepository<Product,Id> {

    List<Product> findByName(String name);
    Product getById(int id);
}
