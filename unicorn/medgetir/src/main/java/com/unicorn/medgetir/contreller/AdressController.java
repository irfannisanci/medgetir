package com.unicorn.medgetir.contreller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unicorn.medgetir.business.abstracts.AdressService;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.AdressDTO;
import com.unicorn.medgetir.model.Adress;

@RestController
@CrossOrigin
@RequestMapping("/Adress")
public class AdressController {
	@Autowired
	AdressService adressService;
	
	@PostMapping("/add-adress")
	public DataResult<Adress> add(@Valid @RequestBody AdressDTO adressDTO) {
		return adressService.add(adressDTO);		
	}
	
	@PutMapping("/updateAddress")
	public DataResult<Adress> updateAddress(@Valid @RequestBody AdressDTO adressDto) {
		return adressService.updateAddress(adressDto);
	}
	
	@DeleteMapping("/deleteAdress")
	public Result deleteAdress(@RequestParam int id) {
		return adressService.deleteAdress(id);
	}
	
	@GetMapping("getAllAdress")
	public DataResult<List<Adress>> getAllAdress(){
		return adressService.getAllAdress();
	}

}
