package com.unicorn.medgetir.dto;



import lombok.Data;
@Data
public class AdressDTO {
	
	private int id;
	private int user_id;
	private String il;
	private String ilce;
	private String mahalle;
	private String cadde;
	private String sokak;

}
