package com.unicorn.medgetir.dto;

import lombok.Data;

@Data
public class PrescriptionsDto {

	private int id;
	private String prescriptionCode;
	private int userId;
	private int chemistId;
}
