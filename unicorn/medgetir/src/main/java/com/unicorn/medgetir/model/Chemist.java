package com.unicorn.medgetir.model;

import java.util.List;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;

import lombok.Data;

@Data
@Entity
public class Chemist {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	private String chemistName;
	private String phone;
	private String password;
	
	@OneToMany(mappedBy = "chemists")
	private List<Prescriptions> prescriptions;
}
