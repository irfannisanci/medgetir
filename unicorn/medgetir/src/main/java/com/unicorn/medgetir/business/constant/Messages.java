package com.unicorn.medgetir.business.constant;


public class Messages {

	public static String getAddress = "Adresler listelendi";
	public static String updateAdress = "Adres bilgisi güncellendi";
	public static String addAdress = "Yeni adres eklendi";
	public static String deleteAdress = "Adres silindi";
	public static String adressNotFound = "Adres bulunamadı";
	public static String createPrescriptions = "Reçete gönderildi";
	public static String existNationalityId = "Bu Tc kimlik numarasına ait kayıt bulunmaktadır";
	public static String existPhone = "Bu telefon numarasına ait kayıt bulunmaktadır";
	public static String createdUser = "Yeni Kullanıcı oluşturuldu";
	public static String userNotFound = "Kullanıcı bulunamadı";
	public static String getAllUsers = "Tüm kullanıcılar listelendi";
	public static String updateUser="Kullanıcı bilgileri güncellendi";
	public static String deleteUser="Kullanıcı silindi";
}
