package com.unicorn.medgetir.dto;

import lombok.Data;

@Data
public class ChemistDto {

	private int id;
	private String chemistName;
	private String phone;
	private String password;
}
