package com.unicorn.medgetir.model;

import java.util.List;
import java.util.Set;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;


import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@Entity
@Table(name ="person")
@AllArgsConstructor
@NoArgsConstructor
@JsonIgnoreProperties({"hibernateLazyInitializer","handler","adress","roles","prescriptions"})
public class User {

	
	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private int id;
	
	@NotBlank(message = "İsim alanı boş olamaz")
	@Size(min=3,message = "İsim 3 karakterden az olamaz")
	private String name;
	
	@NotBlank(message = "Soyad alanı boş olamaz")
	private String surname;
	
	@NotBlank(message = "Şifre alanı boş olamaz")
	@Size(min=6,max=16, message = "Şifre en az 6, en fazla 16 karakterden oluşabilir")
	private String password;
	
	@NotBlank(message = "Tc Kimlik numarası boş olamaz")
	@Size(min=11, max=11, message = "TC Kimlik Numarası 11 hane olmalıdır")
	private String nationalityId;
	
	@NotBlank(message = "Telefon numarası boş olamaz")
	private String phone;
	
	@NotBlank(message = "Doğum yılı alanı boş olamaz")
	@Size(min=4, max=4, message="Doğum yılı 4 karakterden fazla olamaz")
	private String dateOfBirth; 
	
	@OneToMany(mappedBy = "users")
	private List<Prescriptions> prescriptions;
	
	@OneToMany(mappedBy = "users")
	private List<Adress> adress;
	
	 @ManyToMany 
	    @JoinTable( 
	        name = "users_roles", 
	        joinColumns = @JoinColumn (
	        name = "user_id", referencedColumnName = "id"), 
	        inverseJoinColumns = @JoinColumn(
	        name = "role_id", referencedColumnName = "id"))  
	private Set<Role> roles;
}
