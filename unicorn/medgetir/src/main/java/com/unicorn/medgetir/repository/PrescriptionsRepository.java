package com.unicorn.medgetir.repository;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unicorn.medgetir.model.Prescriptions;

public interface PrescriptionsRepository extends JpaRepository<Prescriptions, Integer>{

	List<Prescriptions> getByUsersId(int id);
	List<Prescriptions> getByChemistsId(int id);
}
