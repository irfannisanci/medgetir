package com.unicorn.medgetir.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unicorn.medgetir.model.Chemist;

public interface ChemistRepository extends JpaRepository<Chemist, Integer>{

	Boolean existsByPhone(String phone);
}
