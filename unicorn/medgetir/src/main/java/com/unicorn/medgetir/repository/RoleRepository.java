package com.unicorn.medgetir.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.unicorn.medgetir.model.Role;

public interface RoleRepository extends JpaRepository<Role, Integer> {

}
