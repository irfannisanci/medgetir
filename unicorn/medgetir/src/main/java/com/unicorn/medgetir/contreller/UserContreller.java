package com.unicorn.medgetir.contreller;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PatchMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unicorn.medgetir.business.abstracts.UserService;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.UserDTO;
import com.unicorn.medgetir.model.User;



@RestController
@RequestMapping("/user")
@CrossOrigin
public class UserContreller {
	
	
	@Autowired
	UserService userService;

	
	@PostMapping("/add-user")
	public Result add(@Valid @RequestBody UserDTO userDTO) {
		return userService.add(userDTO);
		}
	
	@GetMapping("/getAllUsers")
	public DataResult<List<User>> getAllUser() {
		return userService.getAllUser();
	}
	
	@GetMapping("/getById")
	public DataResult<User> getById(@RequestParam int id) {
		return userService.getById(id);
	} 
	
	@GetMapping("/findByPhone")
	public DataResult<User> findByPhone(@RequestParam String phone) {
		return userService.findByPhone(phone);
	}
	
	@GetMapping("/findByPhoneAndPassword")
	public DataResult<User> findByPhoneAndPassword(@RequestParam String phone, @RequestParam String password) {
		return userService.findByPhoneAndPassword(phone, password);
	}
	
	
	@PatchMapping("/update")
	public DataResult<User> updateUser(@Valid @RequestBody UserDTO userDTO) {
		return userService.updateUser(userDTO);
	}
	
	@DeleteMapping("deleteUser")
	public Result deleteUser(@RequestParam int id) {
		return userService.deleteUser(id);
	}
	
}
