package com.unicorn.medgetir.business.concretes;

import com.unicorn.medgetir.business.abstracts.ProductService;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.SuccessDataResult;
import com.unicorn.medgetir.dto.ProductDTO;
import com.unicorn.medgetir.model.Product;
import com.unicorn.medgetir.repository.ProductRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProductManager implements ProductService {
    @Autowired
    ProductRepository productRepository;

    @Override
    public DataResult<Product> add(ProductDTO productDTO) {
        Product prodct=new Product();
        prodct.setId(prodct.getId());
        prodct.setName(prodct.getName());
        prodct.setPrice(prodct.getPrice());
        return new SuccessDataResult<Product>(productRepository.save(prodct));
    }

    @Override
    public List<Product> findByName(String name) {

        return productRepository.findByName(name);
    }

	@Override
	public DataResult<Product> updateProduct(ProductDTO productDTO) {
		Product newProduct=productRepository.getById(productDTO.getId());
		newProduct.setName(productDTO.getName());
		newProduct.setPrice(productDTO.getPrice());
		return new SuccessDataResult<Product>(productRepository.save(newProduct));
	}

}
