package com.unicorn.medgetir.business.abstracts;

import java.util.List;

import org.springframework.stereotype.Service;

import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.AdressDTO;
import com.unicorn.medgetir.model.Adress;


@Service
public interface AdressService {

	DataResult<List<Adress>> getAllAdress();
    DataResult<Adress> add(AdressDTO adressDTO);
    DataResult<Adress> updateAddress(AdressDTO adressDto);
    Result findById(int id);
	Result deleteAdress(int id);
	
}
