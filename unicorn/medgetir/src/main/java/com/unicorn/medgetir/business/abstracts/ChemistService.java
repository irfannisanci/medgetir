package com.unicorn.medgetir.business.abstracts;

import java.util.List;

import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.ChemistDto;
import com.unicorn.medgetir.model.Chemist;

public interface ChemistService {

	DataResult<Chemist> createChemist(ChemistDto chemistDto);
	DataResult<List<Chemist>> getAllChemist();
	DataResult<Chemist> updateChemist(ChemistDto chemistDto);
	Result deleteChemist(int id);
}
