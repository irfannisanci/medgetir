package com.unicorn.medgetir.business.abstracts;

import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.dto.ProductDTO;
import com.unicorn.medgetir.model.Product;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public interface ProductService {
    DataResult<Product> add(ProductDTO productDTO);
    DataResult<Product> updateProduct(ProductDTO productDTO);
    List<Product> findByName(String name);
}
