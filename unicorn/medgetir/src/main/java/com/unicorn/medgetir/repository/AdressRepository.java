package com.unicorn.medgetir.repository;



import org.springframework.data.jpa.repository.JpaRepository;

import com.unicorn.medgetir.model.Adress;


public interface AdressRepository extends JpaRepository<Adress,Integer> {

	Adress findById(int id);
	Adress getById(int id);
	//Adress existsById(int id);
	//Adress findAllAdressByUserId(int id);

	
}
