package com.unicorn.medgetir.model;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.validation.constraints.NotBlank;

import lombok.Data;

@Data
@Entity
public class Prescriptions {

	@Id
	@GeneratedValue(strategy =GenerationType.IDENTITY )
	private int id;
	
	@NotBlank(message = "Reçete kodu girilmelidir")
	private String prescriptionCode;
	
	@ManyToOne(targetEntity = User.class)
	@JoinColumn(name = "user_id",referencedColumnName = "id")
	private User users;
	
	@ManyToOne(targetEntity = Chemist.class)
	@JoinColumn(name = "chemist_id",referencedColumnName = "id")
	private Chemist chemists;
}
