package com.unicorn.medgetir.contreller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.unicorn.medgetir.business.abstracts.ChemistService;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.dto.ChemistDto;
import com.unicorn.medgetir.model.Chemist;

@RestController
@RequestMapping("/chemist")
@CrossOrigin
public class ChemistController {

	@Autowired
	private ChemistService chemistService;
	
	@PostMapping("/createChemist")
	public DataResult<Chemist> createChemist(@RequestBody ChemistDto chemistDto){
		return chemistService.createChemist(chemistDto);
	}
	
	@GetMapping("/getAllChemist")
	public DataResult<List<Chemist>> getAllChemist(){
		return chemistService.getAllChemist();
	}
	
	@PutMapping("/updateChemist")
	public DataResult<Chemist> updateChemist(@RequestBody ChemistDto chemistDto){
		return chemistService.updateChemist(chemistDto);
	}
	
	@DeleteMapping("/deleteChemist")
	public Result deleteChemist(@RequestParam int id) {
		return chemistService.deleteChemist(id);
	}
}
