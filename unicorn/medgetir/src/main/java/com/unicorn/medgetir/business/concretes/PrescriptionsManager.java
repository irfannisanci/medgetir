package com.unicorn.medgetir.business.concretes;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.unicorn.medgetir.business.abstracts.PrescriptionsService;
import com.unicorn.medgetir.business.constant.Messages;
import com.unicorn.medgetir.core.utilities.results.DataResult;
import com.unicorn.medgetir.core.utilities.results.Result;
import com.unicorn.medgetir.core.utilities.results.SuccessDataResult;
import com.unicorn.medgetir.core.utilities.results.SuccessResult;
import com.unicorn.medgetir.dto.PrescriptionsDto;
import com.unicorn.medgetir.model.Chemist;
import com.unicorn.medgetir.model.Prescriptions;
import com.unicorn.medgetir.model.User;
import com.unicorn.medgetir.repository.PrescriptionsRepository;

@Service
public class PrescriptionsManager implements PrescriptionsService {

	@Autowired
	private PrescriptionsRepository prescriptionsRepository;
	
	@Override
	public Result addPrescription(PrescriptionsDto prescriptionsDto) {
		User user=new User();
		Chemist chemist=new Chemist();
		Prescriptions newPrescriptions=new Prescriptions();
		newPrescriptions.setId(prescriptionsDto.getId());
		user.setId(prescriptionsDto.getUserId());
		chemist.setId(prescriptionsDto.getChemistId());
		newPrescriptions.setPrescriptionCode(prescriptionsDto.getPrescriptionCode());
		prescriptionsRepository.save(newPrescriptions);
		return new SuccessResult(Messages.createPrescriptions);
	}

	@Override
	public DataResult<List<Prescriptions>> getPrescriptionByUserId(int id) {
		return new SuccessDataResult<List<Prescriptions>>(prescriptionsRepository.getByUsersId(id));
	}

	@Override
	public DataResult<List<Prescriptions>> getPrescriptionByChemistsId(int id) {
		return new SuccessDataResult<List<Prescriptions>>(prescriptionsRepository.getByChemistsId(id));
	}

}
